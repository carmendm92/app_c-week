const mic = document.getElementById('mic');
const screen = document.getElementById('screen');
const panelData = document.getElementById('panelData');

const commands =['mangia', 'balla', 'dormi'];

// inseriamo all'interno della costante l'API per il comando vocale, spcificandop quale utilizzare in caso
// di dipositivo che non lo supporta senza prefisso
const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;

const recog = new SpeechRecognition();

function onStart(){
    panelData.classList.add('listening');
    recog.start();
}

function onResult(e){
    const testo = e.results[0][0].transcript;
    
    const action = commands.find(function(cmd){
        return testo.toLowerCase().includes(cmd);
    })

    const actionClassname = 'codigoci_screen_' + action;
    screen.classList.add(actionClassname);

    panelData.classList.remove('listening');

    setTimeout(function(){
        screen.classList.remove(actionClassname);
    }, 3000);
}

mic.addEventListener('click', onStart);
recog.addEventListener('result', onResult);