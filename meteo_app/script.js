const weatherIcon = document.querySelector('.weather-icon');
const weatherLocation = document.querySelector('.weather-location');
const weatherTemperature = document.querySelector('.weather-temperature');
const suggestionParagraph = document.querySelector('.suggestion');
const panel = document.querySelector('.panel')

// recuperare il tag HTML
const rootElement = document.documentElement;

//Geolocalizzazione
window.navigator.geolocation.getCurrentPosition(onSuccess, onError);

// Funzione da eseguire in caso di errore
function onError(){
    weatherLocation.innerText = "Devi attivare la geolocalizzazione";
    weatherIcon.src = "./images/geolocation_disabled.png"
    panel.classList.add('suggestion_none');
    rootElement.classList.remove("js-loading");
    weatherIcon.classList.add('weather-info');
};

// Funzione da eseguire in caso di successo
function onSuccess(position){
 
    const apiKey = "c7dacda9d79dd6f1e47239cbc14c254f"
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    const lang = "it";
    const units = "metric";
    // indirizzo per la chiamata API
    const endpoint = "https://api.openweathermap.org/data/2.5/weather";
    
    // metodo lungo e noioso
    // const apiUri = endpoint + "?lat=" + latidute + "&long=" + longitude + "&appid=" + apiKey;
    
    const apiUri = `${endpoint}?lon=${longitude}&lat=${latitude}&units=${units}&lang=${lang}&appid=${apiKey}`;
    
    // facciamo la chiamata
    fetch(apiUri)
    // trasformo la response in formato più leggibile (json)
    .then(function(response){
        const data = response.json();
        return data;
    })
    .then(function(data){
        // estrapoliamo le informazioni
        const locationName = data.name;
        const temperature = Math.floor(data.main.temp);
        const iconCode = data.weather[0].icon;
        const description = data.weather[0].description;
        const suggestion = getSuggestion(iconCode);

        weatherLocation.innerText = locationName;
        weatherTemperature.innerText = temperature + "°";
        weatherIcon.alt = description;
        weatherIcon.src = `images/${iconCode}.png`
        suggestionParagraph.innerHTML = suggestion;

        rootElement.classList.remove("js-loading");
    });
};
function getSuggestion(iconCode){
    const suggestions = {
        '01d': 'Ricordati la crema solare!',
        '01n': 'Buonanotte!',
        '02d': 'Oggi il sole va e viene...',
        '02n': 'Attenti ai lupi mannari...',
        '03d': 'Luce perfetta per fare foto!',
        '03n': 'Dormi sereno :)',
        '04d': 'Che cielo grigio :(',
        '04n': 'Non si vede nemmeno la luna!',
        '09d': 'Prendi l\'ombrello',
        '09n': 'Copriti bene!',
        '10d': 'Prendi l\'ombrello',
        '10n': 'Copriti bene!',
        '11d': 'Attento ai fulmini!',
        '11n': 'I lampi accendono la notte!',
        '13d': 'Esci a fare un pupazzo di neve!',
        '13n': 'Notte perfetta per stare sotto il piumone!',
        '50d': 'Accendi i fendinebbia!',
        '50n': 'Guida con prudenza!',
    }
    return suggestions[iconCode];
}
