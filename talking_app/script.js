const textArea = document.querySelector("textarea");
const playButton = document.querySelector("button");
const pitchBar = document.querySelector("input");
const duckFigure = document.querySelector("figure");

playButton.addEventListener("click", function(){
    // trim elimina gli spazi all'inzio e alla fine
    const textLenght = textArea.value.trim().length;
    if(textLenght > 0){
        talk();
    }
})

function talk(){
    const text = textArea.value;
    const pitch = pitchBar.value;

    // crea un nuovo costrutto del sintetizzatore vocale
    const utterance = new SpeechSynthesisUtterance(text);

    // possiamo personalizzare il suono
    // utterance.volume = 1;
    // utterance.rate = 1;

    // personalizziamo la velocità in base al valore che sceglie l'utente
    utterance.pitch = pitch;

    // riproduciamo la frase
    speechSynthesis.speak(utterance);

    utterance.addEventListener('start', function()
    {
        textArea.disabled = true;
        pitchBar.disabled = true;
        playButton.disabled = true;
        duckFigure.classList.add('talking');
    })

    utterance.addEventListener('end', function(){
        textArea.disabled = false;
        pitchBar.disabled = false;
        playButton.disabled = false;
        duckFigure.classList.remove('talking');
    })
}